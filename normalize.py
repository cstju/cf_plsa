# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-07-07 20:46:11
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-12-20 16:30:31

from random import gammavariate
from random import random
import numpy as np
def normalizeVec(vec):
    s = sum(vec)
    assert(abs(s) != 0.0) # the sum must not be 0

    for i in range(len(vec)):
        assert(vec[i] >= 0) # element must be >= 0
        vec[i] = vec[i] * 1.0 / s

def normalizeMatrix(matrix):
    '''Normalize for each row in this matrix.'''
    row_num, column_num = np.shape(matrix)
    for row_index in range(row_num):
        normalizeVec(matrix[row_index])